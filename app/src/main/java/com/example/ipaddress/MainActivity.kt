package com.example.ipaddress

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.net.InetAddress
import java.util.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnHostName.setOnClickListener {
          val  string = etIpAddress.text.toString()
            val thread = Thread {
                try {
                    //Your code goes here
                    val addr = InetAddress.getByName(string)
                    val host = addr.hostName
                    val canonicalHostname: String = addr.getCanonicalHostName()
                    wifiInfo.setText(canonicalHostname)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            thread.start()

        }


    }



}