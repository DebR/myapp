package com.example.ipaddress

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_subnet.*

class SubnetActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_subnet)

        subnetDevicesButton.setOnClickListener{
            Thread(Runnable {
                try {
                    findSubnetDevices()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }).start()
        }

    }

    private fun findSubnetDevices() {

    }
}